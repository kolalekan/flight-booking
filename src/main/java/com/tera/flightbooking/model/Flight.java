package com.tera.flightbooking.model;

import lombok.*;
import javax.persistence.*;

@Entity
@Table
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String flightCode;

    private String airline;

    private String source;

    private String destination;
}
