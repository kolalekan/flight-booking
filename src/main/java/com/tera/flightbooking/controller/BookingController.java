package com.tera.flightbooking.controller;

import com.tera.flightbooking.model.Booking;
import com.tera.flightbooking.service.BookingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/booking")
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Booking bookFlight(@RequestParam("userId") Long userId,
                              @RequestParam("flightCode") String flightCode)  {

        return bookingService.bookFlight(userId, flightCode);
    }
}
