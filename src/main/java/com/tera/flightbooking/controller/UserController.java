package com.tera.flightbooking.controller;

import com.tera.flightbooking.model.User;
import com.tera.flightbooking.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value =  "/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public User addUser(@RequestBody User user)  {
        return userService.saveUser(user);
    }
}
