package com.tera.flightbooking.service;

import com.tera.flightbooking.exception.AppException;
import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser (User user) {
        if (!isEmailValid(user.getEmail())) throw new AppException("Email is invalid");
        return userRepository.save(user);
    }

    public User findUser (Long id) {
        return userRepository.findById(id).orElseThrow(()-> new AppException("Could not find user"));
    }

    public boolean isEmailValid(String email) {
        return email != null && (email.contains("@") && email.contains("."));
    }
}
