package com.tera.flightbooking.service;

import com.tera.flightbooking.exception.AppException;
import com.tera.flightbooking.model.Flight;
import com.tera.flightbooking.repository.FlightRepository;
import org.springframework.stereotype.Service;

@Service
public class FlightService {

    private FlightRepository flightRepository;

    public Flight findFlightByFlightCode(String flightCode) {
        return flightRepository.findByFlightCode(flightCode)
                .orElseThrow(() -> new AppException("Could not find flight using flight code"));
    }
}
