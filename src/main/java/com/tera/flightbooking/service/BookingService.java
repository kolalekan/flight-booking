package com.tera.flightbooking.service;

import com.tera.flightbooking.model.Booking;
import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.BookingRepository;
import org.springframework.stereotype.Service;

@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final UserService userService;

    public BookingService(BookingRepository bookingRepository,
                          UserService userService) {
        this.bookingRepository = bookingRepository;
        this.userService = userService;
    }

    public Booking bookFlight(Long userId, String flightCode) {
        User user = userService.findUser(userId);

        Booking booking = Booking
                .builder()
                .flightCode(flightCode)
                .user(user)
                .build();
        return bookingRepository.save(booking);
    }
}
