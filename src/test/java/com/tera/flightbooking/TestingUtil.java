package com.tera.flightbooking;

import com.tera.flightbooking.model.Booking;
import com.tera.flightbooking.model.Flight;
import com.tera.flightbooking.model.User;

import java.util.Random;
import java.util.UUID;

public class TestingUtil {

    public static String generateRandomString() {
        return UUID.randomUUID().toString();
    }

    public static String generateRandomEmail(){
        return String.format("%s@mail.com", generateRandomString());
    }

    public static Long generateRandomLong() {
        Random random = new Random();
        return Math.abs(random.nextLong());
    }

    public static User generateRandomUser() {
        return User.builder()
                .id(generateRandomLong())
                .email(generateRandomEmail())
                .firstName(generateRandomString())
                .lastName(generateRandomString())
                .build();
    }

    public static Flight generateRandomFlight() {
        return Flight.builder()
                .id(generateRandomLong())
                .airline(generateRandomString())
                .source(generateRandomString())
                .destination(generateRandomString())
                .flightCode(generateRandomString())
                .build();
    }

    public static Booking generateRandomBooking() {
        return Booking.builder()
                .user(generateRandomUser())
                .flightCode(generateRandomString())
                .build();
    }



}
