package com.tera.flightbooking.controller;

import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static com.tera.flightbooking.TestingUtil.generateRandomUser;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = {"com.tera.flightbooking"})
@AutoConfigureMockMvc
@SpringBootTest
class BookingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserRepository userRepository;

    @Test
    void whenValidInput_thenReturns200() throws Exception {
        User user = generateRandomUser();

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        mockMvc.perform(post("/booking/create")
                .contentType("application/json")
                .param("userId", user.getId().toString())
                .param("flightCode", "CODE"))
                .andExpect(status().isOk());
    }

    @Test
    void whenValidInput_thenReturns400_whenUserIdAbsent() throws Exception {
        User user = generateRandomUser();
        when(userRepository.save(any(User.class))).thenReturn(user);

        mockMvc.perform(post("/booking/create")
                .contentType("application/json")
                .param("flightCode", "CODE"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenValidInput_thenReturns400_whenFlightCodeAbsent() throws Exception {
        User user = generateRandomUser();
        when(userRepository.save(any(User.class))).thenReturn(user);

        mockMvc.perform(post("/booking/create")
                .contentType("application/json")
                .param("userId", user.getId().toString()))
                .andExpect(status().isBadRequest());
    }
}