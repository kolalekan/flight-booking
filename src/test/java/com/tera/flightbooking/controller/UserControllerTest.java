package com.tera.flightbooking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = {"com.tera.flightbooking"})
@AutoConfigureMockMvc
@SpringBootTest
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    UserRepository userRepository;

    @Test
    void whenValidInput_thenReturns200() throws Exception {
        User user = User.builder()
                .email("test@test.com")
                .firstName("Random")
                .lastName("Name")
                .build();

        when(userRepository.save(user)).thenReturn(user);

        mockMvc.perform(post("/user/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    @Test
    void whenValidInput_thenReturns400_whenRequestBodyAbsent() throws Exception {
        mockMvc.perform(post("/user/create")
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenValidInput_thenReturnsServerError_whenEmailInvalid() throws Exception {
        User user = User.builder()
                .email("test@testcom")
                .firstName("Random")
                .lastName("Name")
                .build();

        when(userRepository.save(any(User.class))).thenReturn(user);

        mockMvc.perform(post("/user/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().is5xxServerError());
    }
}
