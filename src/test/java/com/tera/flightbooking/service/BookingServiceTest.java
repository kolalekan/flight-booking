package com.tera.flightbooking.service;

import com.tera.flightbooking.model.Booking;
import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.BookingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

class BookingServiceTest {

    private final static String FLIGHT_CODE = "BA 154";

    private final BookingRepository bookingRepository = Mockito.mock(BookingRepository.class);
    private final UserService userService = Mockito.mock(UserService.class);

    private BookingService bookingService;

    @BeforeEach
    void setup() {
        this.bookingService = new BookingService(bookingRepository, userService);
    }

    @Test
    void test_bookFlight_returnsBooking() {
        Booking booking = booking();
        when(userService.findUser(1L)).thenReturn(user());
        when(bookingRepository.save(any(Booking.class))).thenReturn(booking);

        Booking savedBooking = bookingService.bookFlight(1L, FLIGHT_CODE);
        assertThat(savedBooking).isNotNull();
    }

    private Booking booking() {
        return Booking.builder()
                .flightCode(FLIGHT_CODE)
                .user(user())
                .build();
    }

    private User user() {
        return User.builder()
                .id(1L)
                .firstName("Apt")
                .lastName("Team")
                .email("pro@fec.tus")
                .build();
    }
}