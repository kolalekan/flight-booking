package com.tera.flightbooking.service;

import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserRepository userRepository;

    private UserService service;

    @BeforeEach
    void setup() {
        this.service = new UserService(userRepository);
    }

    @Test
    void test_saveUser_returnsUser() {
        when(userRepository.save(any(User.class))).thenReturn(user());
        User savedUser = service.saveUser(user());
        assertThat(savedUser).isNotNull();
        assertEquals(savedUser.getEmail(), user().getEmail());
    }

    @Test
    void test_isEmailValid_methodReturnsTrue(){
        boolean isEmailValid = service.isEmailValid("test@test.com");
        assertTrue(isEmailValid);
    }

    @Test
    void test_isEmailValid_methodReturnsFalse(){
        boolean isEmailValid = service.isEmailValid("test.com");
        assertFalse(isEmailValid);
    }

    private User user() {
        return User.builder()
                .email("test@test.com")
                .firstName("Team")
                .build();
    }
}