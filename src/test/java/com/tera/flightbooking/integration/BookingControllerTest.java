package com.tera.flightbooking.integration;

import com.tera.flightbooking.model.User;
import com.tera.flightbooking.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.tera.flightbooking.TestingUtil.generateRandomLong;
import static com.tera.flightbooking.TestingUtil.generateRandomUser;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class BookingControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .build();
    }

    @Test
    void whenValidInput_thenReturns200() throws Exception {
        User user = generateRandomUser();
        User createdUser = userRepository.save(user);

        mockMvc.perform(post("/booking/create")
                .contentType("application/json")
                .param("userId", createdUser.getId().toString())
                .param("flightCode", "CODE"))
                .andExpect(status().isOk());
    }

    @Test
    void whenValidInput_thenReturns500_whenUserDoesNotExist() throws Exception {
        mockMvc.perform(post("/booking/create")
                .contentType("application/json")
                .param("userId", generateRandomLong().toString())
                .param("flightCode", "CODE"))
                .andExpect(status().is5xxServerError());
    }
}
