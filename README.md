# Documentation of Flight App to demonstrate testing in Spring boot applications

### Forms of Testing demonstrated
### - Unit Testing
### - Controller Testing
### - Integration Testing
<br />

### Set up Project
#### - Ensure MYSQL Running on port "3306"
#### - CREATE DATABASE "FlightBooking"
#### - The username and password to the database "FlightBooking" should be updated in the application.properties file
#### - You should be up and running
